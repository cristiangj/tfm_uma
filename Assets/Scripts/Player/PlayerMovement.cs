﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMovement : MonoBehaviour
{

	[Header("Movement parameters")] 
	public string horizontal;
	public string vertical;
	[Range(0f, 1f)] public float walkLimit = 0.5f;
	[Range(0, 100)] public float walkVelocity, runVelocity, rotationVelocity;

	private float _horizontalValue, _verticalValue;
	private Vector3 _moveDirection = Vector3.zero;
	private float _actualVelocity;
	private Vector3 _directionToLook = Vector3.zero;
	private Rigidbody _rb;
	private bool _first = false;
	//private PlayerAnimation _playerAnimation;
	//private Coroutine _idleCoroutine;


	void Awake() {
		//_playerAnimation = GetComponentInChildren<PlayerAnimation>();
		_rb = GetComponent<Rigidbody>();
	}

	private void FixedUpdate() {

		_horizontalValue = Input.GetAxis(horizontal);
		_verticalValue = Input.GetAxis(vertical);

		if (Mathf.Abs(_horizontalValue) > 0.0f || Mathf.Abs(_verticalValue) > 0.0f) {
			MoveCharacter();
			_first = true;
		} else if (_first) {
			_rb.velocity = new Vector3(0f,_rb.velocity.y,0f);
			_first = false;
		}

	}

	private void MoveCharacter() {

		//if (_idleCoroutine != null) {
		//	StopCoroutine(_idleCoroutine);
		//}

		SetMovementDirection();
		SetVelocity();
		CalculateRotation();
		Move();
		Rotate();

		//_idleCoroutine = StartCoroutine(WaitForIdle());
	}


	/* PRIVATE METHODS */

	private void SetMovementDirection() {
		//normalize vector to avoid go faster in diagonal
		_moveDirection = (_horizontalValue * Vector3.right + _verticalValue * Vector3.forward).normalized;
	}

	private void SetVelocity() {
		if (Mathf.Abs(_horizontalValue) < walkLimit & Mathf.Abs(_verticalValue) < walkLimit) {
			_actualVelocity = walkVelocity;
			//_playerAnimation.Walk();
		}
		else {
			_actualVelocity = runVelocity;
			//_playerAnimation.Run();
				
		}
	}

	private void CalculateRotation() {
		float angle = Mathf.Atan2(_horizontalValue, _verticalValue) * Mathf.Rad2Deg;
		_directionToLook.y = angle;
	}

	private void Move() {
		//_moveDirection * _actualVelocity);
		_rb.velocity = _moveDirection * _actualVelocity;
	}

	private void Rotate() {
		_rb.rotation = Quaternion.Euler(_directionToLook);
	}

	private IEnumerator WaitForIdle() {

		yield return new WaitForSecondsRealtime(0.15f);

		//_playerAnimation.Idle();
	}

}
