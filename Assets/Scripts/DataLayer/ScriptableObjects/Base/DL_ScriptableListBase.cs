﻿using System.Collections.Generic;

namespace FOURSG.Modules.DataLayer.Base {

	public class DL_ScriptableListBase<T> : DL_ScriptableObjectBase<List<T>> {

		public override void SetValue(List<T> value) {
			this.value = new List<T>(value);
		}
	}

}
