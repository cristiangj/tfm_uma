﻿using UnityEngine;

namespace FOURSG.Modules.DataLayer.Base {

	public class DL_ScriptableObjectBase<T> : ScriptableObject {

		public T value;

		public virtual void SetValue(T value) {
			this.value = value;
		}

		public virtual T GetValue() {
			return value;
		}

	}
}
