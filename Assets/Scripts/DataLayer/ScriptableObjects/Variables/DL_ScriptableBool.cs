﻿using UnityEngine;

namespace FOURSG.Modules.DataLayer.Variables {

	[CreateAssetMenu(menuName = "DataLayer/Scriptable Objects/Variables/New Scriptable Bool")]
	public class DL_ScriptableBool : Base.DL_ScriptableObjectBase<bool> {
		
	}
}
