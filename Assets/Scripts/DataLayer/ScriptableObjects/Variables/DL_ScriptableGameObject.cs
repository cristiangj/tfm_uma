﻿using UnityEngine;

namespace FOURSG.Modules.DataLayer.Variables {

	[CreateAssetMenu(menuName = "DataLayer/Scriptable Objects/Variables/New Scriptable GameObject")]
	public class DL_ScriptableGameObject: Base.DL_ScriptableObjectBase<GameObject> {
	
	}
}
