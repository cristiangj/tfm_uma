﻿using UnityEngine;

namespace FOURSG.Modules.DataLayer.Variables {

	[CreateAssetMenu(menuName = "DataLayer/Scriptable Objects/Variables/New Scriptable Int")]
	public class DL_ScriptableInt : Base.DL_ScriptableObjectBase<int> {
		
	}
}
