﻿using UnityEngine;

namespace FOURSG.Modules.DataLayer.Variables {

	[CreateAssetMenu(menuName = "DataLayer/Scriptable Objects/Variables/New Scriptable Float")]
	public class DL_ScriptableFloat : Base.DL_ScriptableObjectBase<float> {
		
	}
}
