﻿using UnityEngine;

namespace FOURSG.Modules.DataLayer.Variables {

	[CreateAssetMenu(menuName = "DataLayer/Scriptable Objects/Variables/New Scriptable Player Stats")]
	public class DL_ScriptablePlayerStats : Base.DL_ScriptableObjectBase<Classes.PlayerStats> {
		
	}
}
