﻿using UnityEngine;

namespace FOURSG.Modules.DataLayer.Lists { 

	[CreateAssetMenu( menuName = "DataLayer/Scriptable Objects/Lists/New Scriptable List Transform")]
	public class DL_ScriptableListGameTransform : Base.DL_ScriptableListBase<Transform> {
	
	}

}
