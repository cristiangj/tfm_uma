﻿using UnityEngine;

namespace FOURSG.Modules.DataLayer.Lists { 

	[CreateAssetMenu( menuName = "DataLayer/Scriptable Objects/Lists/New Scriptable List GameObject")]
	public class DL_ScriptableListGameObject : Base.DL_ScriptableListBase<GameObject>
	{
	
	}

}
