﻿using UnityEngine;

namespace FOURSG.Modules.DataLayer.Lists {

	[CreateAssetMenu(menuName = "DataLayer/Scriptable Objects/Lists/New Scriptable List Float")]
	public class DL_ScriptableListFloat : Base.DL_ScriptableListBase<float> {

	}
}
