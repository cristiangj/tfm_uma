﻿using UnityEngine;

namespace FOURSG.Modules.DataLayer.Lists { 

	[CreateAssetMenu( menuName = "DataLayer/Scriptable Objects/Lists/New Scriptable List Int")]
	public class DL_ScriptableListInt : Base.DL_ScriptableListBase<int> {
	
	}

}
