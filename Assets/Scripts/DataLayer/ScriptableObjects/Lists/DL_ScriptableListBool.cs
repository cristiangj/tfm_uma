﻿using UnityEngine;

namespace FOURSG.Modules.DataLayer.Lists { 

	[CreateAssetMenu( menuName = "DataLayer/Scriptable Objects/Lists/New Scriptable List Bool")]
	public class DL_ScriptableListBool : Base.DL_ScriptableListBase<bool> {
	
	}

}
